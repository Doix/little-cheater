#include "lf2manager.h"
#include "lf2.h"
#include <QThread>


LF2Manager::LF2Manager(QObject *parent) :
    QObject(parent)
{
}

template <class tType> tType ReadMemoryType(HANDLE hProcess, DWORD pLocaton)
{
    tType pType = {0};
    ReadProcessMemory(hProcess,(LPCVOID)pLocaton,&pType,sizeof(tType),NULL);
    return pType;
}

template <class tType> BOOL WriteMemoryType(HANDLE hProcess,tType* pType ,void* pLocaton)
{
    return WriteProcessMemory(hProcess,pLocaton,pType,sizeof(tType),NULL);
}

BOOL LF2Manager::GetProcessID(char* szWndName, unsigned long* pPID)
{
    HWND hWnd = FindWindowA(szWndName,NULL);
    if(hWnd != NULL)
        if(GetWindowThreadProcessId(hWnd,pPID))
            return TRUE;

    return FALSE;
}

HANDLE LF2Manager::GetLF2Process()
{
    DWORD dwPID;
    GetProcessID("Marti",&dwPID);

    HANDLE hToken = 0;
    LUID pLUID;
    TOKEN_PRIVILEGES pTokenPrivileges;
    if(!OpenProcessToken(GetCurrentProcess(),TOKEN_ADJUST_PRIVILEGES|TOKEN_QUERY,&hToken))
        return NULL;

    if(LookupPrivilegeValue(NULL,SE_DEBUG_NAME,&pLUID))
    {
        pTokenPrivileges.PrivilegeCount = 1;
        pTokenPrivileges.Privileges[0].Luid = pLUID;
        pTokenPrivileges.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
        AdjustTokenPrivileges(hToken,FALSE,&pTokenPrivileges,sizeof(pTokenPrivileges),NULL,NULL);
    }

    CloseHandle(hToken);
    return OpenProcess(PROCESS_VM_OPERATION|PROCESS_VM_READ|PROCESS_QUERY_INFORMATION|PROCESS_VM_WRITE,FALSE,dwPID);
}

void LF2Manager::doWork()
{
    while (true) {
        HANDLE hProcess = GetLF2Process();

        emit requestClearItems();
        char exists[400] = {0};

        ReadProcessMemory(hProcess,(void *)0x458B04,&exists,400,NULL);

        for(int i=0; i<400; i++)
        {
            if (exists[i] == 1) {
                QTreeWidgetItem * item = new QTreeWidgetItem();
                DWORD pObject = ReadMemoryType<DWORD>(hProcess,(DWORD)0x458B00+0x194+i*4);
                sObject object = ReadMemoryType<sObject>(hProcess,(DWORD)pObject);

                sDataFile dataFile = ReadMemoryType<sDataFile>(hProcess,(DWORD)object.data);
                item->setText(0,QString::number(i));
                item->setText(1,QString::number(dataFile.id));
                item->setText(2,QString::fromLocal8Bit(dataFile.name));
                emit requestAddItem(item);
            }
        }
        QThread::msleep(50);
    }
}

