#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ui_mainwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void clearItems();
    void addItem(QTreeWidgetItem *);

signals:


private:
    Ui::MainWindow *ui;
    void updateList();
};

#endif // MAINWINDOW_H
