#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "lf2.h"
#include <QDebug>
#include <QThread>
#include <QString>
#include "lf2manager.h"

void MainWindow::clearItems()
{
    ui->treeWidget->clear();
}

void MainWindow::addItem(QTreeWidgetItem * item)
{
    ui->treeWidget->addTopLevelItem(item);
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QThread *thread = new QThread( );
    LF2Manager    *lf2Manager   = new LF2Manager();
    lf2Manager->moveToThread(thread);
    connect( thread, SIGNAL(started()), lf2Manager, SLOT(doWork()) );
    connect( lf2Manager, SIGNAL(workFinished()), thread, SLOT(quit()) );
    //automatically delete thread and task object when work is done:
    connect( thread, SIGNAL(finished()), lf2Manager, SLOT(deleteLater()) );
    connect( thread, SIGNAL(finished()), thread, SLOT(deleteLater()) );
    //update list
    connect(lf2Manager,SIGNAL(requestAddItem(QTreeWidgetItem*)),this,SLOT(addItem(QTreeWidgetItem*)));
    connect(lf2Manager,SIGNAL(requestClearItems()),this,SLOT(clearItems()));

    thread->start();

}

MainWindow::~MainWindow()
{
    delete ui;
}
