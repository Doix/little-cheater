#-------------------------------------------------
#
# Project created by QtCreator 2013-12-30T16:24:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lf2Obj
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    lf2manager.cpp

HEADERS  += mainwindow.h \
    lf2.h \
    lf2manager.h

FORMS    += mainwindow.ui

msvc:LIBS += -L"C:/Program Files (x86)/Microsoft SDKs/Windows/v7.1A/Lib" -lPsapi -lKernel32 -lAdvapi32
