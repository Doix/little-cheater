#ifndef LF2MANAGER_H
#define LF2MANAGER_H

#include <QObject>
#include "ui_mainwindow.h"
#include <Windows.h>

class LF2Manager : public QObject
{
    Q_OBJECT
public:
    explicit LF2Manager(QObject *parent = 0);

public slots:
    void doWork();

signals:
    void workFinished();
    void requestAddItem(QTreeWidgetItem *);
    void requestClearItems();

private:
    BOOL GetProcessID(char* szWndName, unsigned long* pPID);
    HANDLE GetLF2Process();

};

#endif // LF2MANAGER_H
